package com.example.nishant.mobileauth;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

public class MainActivity extends AppCompatActivity {
    private Button buttonSend,buttonVerifyOTP;
    private EditText editTextPhoneNumber,editTextVerifyOTP;
   // private  FirebaseFirestore db = FirebaseFirestore.getInstance();
    private static final String TAG = "PhoneLogin";
    private boolean mVerificationInProgress = false;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonSend = (Button)findViewById(R.id.buttonSend);
        buttonVerifyOTP = (Button)findViewById(R.id.buttonVerifyOTP);
        editTextPhoneNumber =(EditText)findViewById(R.id.editTextPhoneNumber);
        editTextVerifyOTP = (EditText)findViewById(R.id.editTextVerifyOTP);
        mAuth = FirebaseAuth.getInstance(); // Initialize firebase auth

        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:" + phoneAuthCredential);
                mVerificationInProgress = false;
                Toast.makeText(MainActivity.this,"Verification Complete",Toast.LENGTH_SHORT).show();
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                Toast.makeText(MainActivity.this,"Verification Failed",Toast.LENGTH_SHORT).show();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    Toast.makeText(MainActivity.this, "InValid Phone Number", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // ...
            }

        };

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phoneNumber = editTextPhoneNumber.getText().toString().trim();

                if(TextUtils.isEmpty(phoneNumber)){
                    Toast.makeText(MainActivity.this, "Please enter your mobile number", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d(TAG, "phoneNumber = " + phoneNumber);

                PhoneAuthProvider.getInstance().verifyPhoneNumber(
                        phoneNumber,
                        60,
                        java.util.concurrent.TimeUnit.SECONDS,
                        MainActivity.this,
                        mCallbacks);

            }
        });// button Phone number


        buttonVerifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String OTP =  editTextVerifyOTP.getText().toString();
                if(TextUtils.isEmpty(OTP)){
                    Toast.makeText(MainActivity.this, "Please enter the OPT you received", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.d(TAG, "OTP = " + OTP);
                PhoneAuthCredential phonecredential = PhoneAuthProvider.getCredential(mVerificationId, OTP);
                // [END verify_with_code]
                signInWithPhoneAuthCredential(phonecredential);
            }

        });// button verify


    }//end of on create

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Log.d(TAG, "signInWithCredential:success");
                            // startActivity(new Intent(PhoneLogin.this,Home.class));
                            Toast.makeText(MainActivity.this,"Verification Done",Toast.LENGTH_SHORT).show();
                            FirebaseUser user = task.getResult().getUser();
                            startActivity(new Intent( getApplicationContext() , UserActivity.class));
                            finish();
                            // ...
                        }
                        else {
                            // Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                Toast.makeText(MainActivity.this,"Invalid Verification",Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

}
